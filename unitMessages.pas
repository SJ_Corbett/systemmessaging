unit unitMessages;

interface

uses
  System.Messaging, FMX.Types;

type
  TMessageSwitch = class(TMessage<Boolean>);
  TMessageLog = class(TMessage<string>);
  TMessageMyObject = class(TMessage<TObject>);
  TMessageSendStatus = class(TMessage);
  // Do not define messages like this!!
  TMessageString1 =  TMessage<string>;
  TMessageString2 =  TMessage<string>;
implementation

end.

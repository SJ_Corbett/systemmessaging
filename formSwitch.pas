unit formSwitch;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes,
  System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.StdCtrls,
  FMX.Controls.Presentation;

type
  TfrmSwitch = class(TForm)
    Switch1: TSwitch;
    Label1: TLabel;
    btnClose: TButton;
    Panel1: TPanel;
    procedure btnCloseClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Switch1Switch(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    fSwitchMessageId: integer;
    fSwitching: Boolean;
    fSendStatusMessageId: integer;
  public
    { Public declarations }
  end;

implementation

{$R *.fmx}

uses
  System.Messaging, unitMessages;

procedure TfrmSwitch.btnCloseClick(Sender: TObject);
begin
  Self.Close;
end;

procedure TfrmSwitch.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  TMessageManager.DefaultManager.Unsubscribe(TMessageSwitch, fSwitchMessageId);
  TMessageManager.DefaultManager.Unsubscribe(TMessageSendStatus,
    fSendStatusMessageId);
  Action := TCloseAction.caFree;
end;

procedure TfrmSwitch.FormCreate(Sender: TObject);
begin
  Self.Caption := Format('Switch Form %d', [Random(999)]);

  fSwitchMessageId := TMessageManager.DefaultManager.SubscribeToMessage
    (TMessageSwitch,
    procedure(const Sender: TObject; const M: TMessage)
    begin
      if (Sender <> Self) then
      begin
        fSwitching := True;
        Switch1.IsChecked := (M as TMessageSwitch).Value;
        fSwitching := False;
      end;
    end);
  fSwitching := False;

  fSendStatusMessageId := TMessageManager.DefaultManager.SubscribeToMessage
    (TMessageSendStatus,
    procedure(const Sender: TObject; const M: TMessage)
    begin
      if (Sender <> Self) then
        Switch1Switch(Sender);
    end);

  TMessageManager.DefaultManager.SendMessage(Self, TMessageSendStatus.Create);
end;

procedure TfrmSwitch.Switch1Switch(Sender: TObject);
begin
  if not fSwitching then
    TMessageManager.DefaultManager.SendMessage(Self,
      TMessageSwitch.Create(Switch1.IsChecked));
end;

end.

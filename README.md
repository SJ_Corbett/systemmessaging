# SystemMessaging #

This Delphi project is to demonstrate the use of the System.Messaging unit and how it can be used to implement messaging within an application.

## What Is System.Messaging? ##

System.Messaging is part of the runtime library of recent versions of Delphi so is available for VCL and Firemonkey applications.  It used to broadcast "stuff" between elements of an application.  What sort of stuff...

 * A message to say that something has happened or is about to happen.
 * A message containing a value (string, number, Boolean, etc).
 * A message containing an object.

System.Messaging is not a method of communicating between applications on the same or different computer.

Using System.Messaging can reduce or eliminate dependencies and coupling between units which is always a good thing.  The sender of the message does not need to know anything about the receiver.  Likewise, the receiver does not need to know anything about the sender of the message.

Refer to the file "Use Of System.Messaging in Firemonkey.txt" in this project for a list of Delphi units that refer to System.Messaging.

## Practical Uses For System.Messaging ##

 * Synchronise the status of an object on one screen when changes are made to that object on another.
 * Log messages or debugging information.
 * Unit testing.

## Steps To Use System.Messaging ##

Define the messages that you wish to use as sub classes of the TMessage type.  To avoid circular references issues I would recommend placing these in their own unit, UnitMessages.pas in this project.
 
     type
       TMyMessage = class(TMessage<Boolean>);

In the units where you wish to send or respond to messages add the System.Messaging unit.  My preference is to always place references to units in the implementation section whenever possible.

You can create your own global message manager though normally you'd use the default instance, TMessageManager.DefaultManager. 

Sending a message is as easy as:

    TMessageManager.DefaultManager.SendMessage(Self, TMyMessage.Create(True));

To respond to a message you must first subscribe to receive the message or messages that you're interested in and to specify a procedure to run when a message is received.  This can either be a regular or anonymous method:
	
    fMyMessageId := TMessageManager.DefaultManager.SubscribeToMessage
      (TMyMessage,
      procedure(const Sender: TObject; const M: TMessage)
      begin
          if (Sender <> Self) then
            fBoolean := (M as TMyMessage).Value;
      end);

Any unit responding to messages should unsubscribe to the messages before being destroyed using the integer ID obtained when subscribing to the message:

    TMessageManager.DefaultManager.Unsubscribe(TMyMessage, fMyMessageId);

## Other Notes ##

 * The invocation of the receiver methods is synchronous to the SendMessage method, all receiver methods will complete prior to the next statement executing.
 * The invocation of the receiver methods is in the same thread as the SendMessage method.  This might be a reason to create an application based message manager rather than use the "TMessageManager.DefaultManager".
 * Subscribing to and unsubscribing is not thread safe.

## Versions ##

This project should work with all versions that implement the Systems.Messaging unit.  It has been tested with Delphi Seattle, Berlin and Tokyo.

I do not know when the System.Messaging unit was introduced into Delphi but it does not compile under Delphi XE5 or earlier.

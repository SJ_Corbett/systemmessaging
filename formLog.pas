unit formLog;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes,
  System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.StdCtrls,
  FMX.Controls.Presentation, FMX.ScrollBox, FMX.Memo, System.Messaging,
  FMX.Objects;

type
  TfrmLog = class(TForm)
    Memo1: TMemo;
    btnClose: TButton;
    procedure btnCloseClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure FormHide(Sender: TObject);
  private
    fSwitchMessageId: integer;
    fLogMessageId: integer;
    fMessageId: integer;
    fSendStatusMessageId: Integer;
    procedure ReceiveMessage(const Sender: TObject; const M: TMessage);
  public
    { Public declarations }
  end;

var
  frmLog: TfrmLog;

implementation

{$R *.fmx}

uses unitMessages;

procedure TfrmLog.btnCloseClick(Sender: TObject);
begin
  Self.Close;
end;

procedure TfrmLog.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  TMessageManager.DefaultManager.Unsubscribe(TMessageSwitch, fSwitchMessageId, True);
  TMessageManager.DefaultManager.Unsubscribe(TMessageLog, fLogMessageId, True);
  TMessageManager.DefaultManager.Unsubscribe(TMessageMyObject, fMessageId, True);
  TMessageManager.DefaultManager.Unsubscribe(TMessageSendStatus, fSendStatusMessageId, True);

  TMessageManager.DefaultManager.SendMessage(Self,
    TMessageLog.Create('Closing Form'));
  Action := TCloseAction.caFree;
end;

procedure TfrmLog.FormCreate(Sender: TObject);
begin
  Self.Caption := Format('Log Form %d', [Random(999)]);
  fSwitchMessageId := TMessageManager.DefaultManager.SubscribeToMessage
    (TMessageSwitch, ReceiveMessage);
  fLogMessageId := TMessageManager.DefaultManager.SubscribeToMessage
    (TMessageLog, ReceiveMessage);
  fMessageId := TMessageManager.DefaultManager.SubscribeToMessage
    (TMessageMyObject, ReceiveMessage);
  fSendStatusMessageId := TMessageManager.DefaultManager.SubscribeToMessage
    (TMessageSendStatus, ReceiveMessage);

  TMessageManager.DefaultManager.SendMessage(Self,
    TMessageLog.Create('Opening Form'));
end;

procedure TfrmLog.FormHide(Sender: TObject);
begin
  Self.Close;
end;

procedure TfrmLog.ReceiveMessage(const Sender: TObject; const M: TMessage);
var
  ws: string;
begin
  if Sender = Self then
    Exit;

  ws := Format('Received a %s from ', [M.ClassName]);
  if Sender is TForm then
    ws := ws + TForm(Sender).Caption
  else
    ws := Format('%s a %s object', [ws, Sender.ClassName]);

  if M is TMessageSwitch then
    ws := Format('%s. Value = %s', [ws, BoolToStr(TMessageSwitch(M).Value)])
  else if M is TMessageLog then
    ws := Format('%s. Value = %s', [ws, TMessageLog(M).Value])
  else if (M is TMessageMyObject) and (TMessageMyObject(M).Value is TComponent) then
    ws := Format('%s a %s called %s', [ws, TMessageMyObject(M).Value.ClassName,
      TComponent(TMessageMyObject(M).Value).Name]);

  Memo1.Lines.Add(ws);
end;

end.

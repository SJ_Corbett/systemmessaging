unit formTraffic;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes,
  System.Variants, System.Messaging,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.Effects,
  FMX.Objects, FMX.Layouts, FMX.Controls.Presentation, FMX.StdCtrls, FMX.Ani;

type
  TfrmTraffic = class(TForm)
    CircleRed: TCircle;
    CircleGreen: TCircle;
    btnClose: TButton;
    GridPanelLayout1: TGridPanelLayout;
    GlowEffect1: TGlowEffect;
    faCircleRed: TFloatAnimation;
    faCircleGreen: TFloatAnimation;
    procedure FormCreate(Sender: TObject);
    procedure btnCloseClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure CircleClick(Sender: TObject);
  private
    fSwitchMessageId: integer;
    procedure ReceiveSwitchMessage(const Sender: TObject; const M: TMessage);
  public
    { Public declarations }
  end;

implementation

{$R *.fmx}

uses
  unitMessages;

procedure TfrmTraffic.btnCloseClick(Sender: TObject);
begin
  CircleClick(Sender);
  Self.Close;
end;

procedure TfrmTraffic.CircleClick(Sender: TObject);
begin
  TMessageManager.DefaultManager.SendMessage(Self,
    TMessageMyObject.Create(Sender));
end;

procedure TfrmTraffic.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  TMessageManager.DefaultManager.Unsubscribe(TMessageSwitch, fSwitchMessageId);
  TMessageManager.DefaultManager.SendMessage(Self,
    TMessageLog.Create('Closing Form'));
  Action := TCloseAction.caFree;
end;

procedure TfrmTraffic.FormCreate(Sender: TObject);
begin
  Self.Caption := Format('Lights Form %d', [Random(999)]);
  TMessageManager.DefaultManager.SendMessage(Self,
    TMessageLog.Create('Opening Form'));
  fSwitchMessageId := TMessageManager.DefaultManager.SubscribeToMessage
    (TMessageSwitch, ReceiveSwitchMessage);

  TMessageManager.DefaultManager.SendMessage(Self, TMessageSendStatus.Create);
end;

procedure TfrmTraffic.ReceiveSwitchMessage(const Sender: TObject;
  const M: TMessage);
var
  wkMessageSwitch: TMessageSwitch;
begin
  if (M is TMessageSwitch) then
  begin
    wkMessageSwitch := TMessageSwitch(M);
    faCircleRed.Enabled := False;
    faCircleRed.StartValue := CircleRed.Opacity;
    faCircleGreen.Enabled := False;
    faCircleGreen.StartValue := CircleGreen.Opacity;

    if wkMessageSwitch.Value then
    begin
      faCircleRed.StopValue := 0.3;
      faCircleGreen.StopValue := 1;
      GlowEffect1.Parent := CircleGreen;
      TMessageManager.DefaultManager.SendMessage(Self,
        TMessageLog.Create('Light is now Green'));
    end
    else
    begin
      faCircleRed.StopValue := 1;
      faCircleGreen.StopValue := 0.3;
      GlowEffect1.Parent := CircleRed;
      TMessageManager.DefaultManager.SendMessage(Self,
        TMessageLog.Create('Light is now Red'));
    end;
    faCircleRed.Enabled := True;
    faCircleGreen.Enabled := True;
  end;
end;

end.
